#!/bin/bash

### Prepareation, transform the dataset
PYDIR="$(pwd)/code/py/"
TRANSFORMFILE="trasformDataset.py"

### Turn on the server
SERVERFILE="server.py"

cd $PYDIR
python $TRANSFORMFILE
python $SERVERFILE