# Database project

## Introduction

This is a **database**-driven project (application) that uses *Python* and *MySQL* with a webpage as the user interface. The primary purpose of this project is to learn how to embed a database into an application. Furthermore, instead of setting up the environment for the application manually, this project is also using **docker** as an easier way to build and run the application.

## Things learned through this project

1. Grasped the knowledge of **database design** decisions (including an E/R diagram)
2. Gained more experience and knowledge of using a **DBMS** (database management system) with customized code (written in Python)
3. Combined the knowledge of a DBMS and **software development** to make an application
4. Practiced **coding skills** with Python (including flask)
5. Acquired practical application of **docker** in deploying an application (locally)

## E/R diagram

![avatar](/ER/ER.jpg)

## Commands

Some basic commands learned by building up this project.

#### MySQL commands

- CREATE database [database name];
- DROP database [database name];
- SHOW databases;
- SELECT version(); # show MySQL version
- SELECT now(); # show current time
- CREATE Table [table name]
- SELECT ColumnName1, ColumnName2,... FROM Table1
- INSERT INTO Table1 (ColumnName1, ColumnName2,...)

Example of creating and loading a table with MySQL using cli and vim:

insert the following code to table.sql and save.

```
CREATE TABLE student
  (
     snum     DECIMAL(9, 0) NOT NULL PRIMARY KEY,
     sname    VARCHAR(30),
     major    VARCHAR(25),
     standing VARCHAR(2),
     age      DECIMAL(3, 0)
  );
```

Use the file to create table within mysql envrionment:
`mysql> source table.sql;`

## Run the application

This project provided two ways to help you set up the application easily. One method is to [install the application locally](#setup-locally), and the other one is to [use docker-compose to build the application](#using-docker-to-run-the-application).

### Setup Locally

We need the following criteria as preconditions for this application:

1. Python and the package `pymysql` are installed on locally
2. MySQL is installed
3. A database with the following settings is created within MySQL
   ```
   host='localhost',
   user='dbSalary',
   password='dbSalary',
   db='dbSalary',
   port=3306,
   charset='utf8'
   ```

We can achieve part 3 by following the steps below using `MySQL` command line environment:

1. Create database `dbSalary` with default charset of `utf8`:
   
   ```
   mysql> Create DATABASE IF NOT EXISTS dbSalary DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
   ```
2. Add user info for user `dbSalary` and use `localhost` as host:
   
   ```
   mysql> use mysql;
   
   mysql> INSERT INTO mysql.user
   (host, user, ssl_cipher, x509_issuer, x509_subject)
   VALUES('localhost','dbSalary',"","","");
   ```
3. Add password (`password='dbSalary'`) for this user:
   
   MySQL version <= 5.6.27
   
   ```
   mysql> update user set password = password(“dbSalary”) where user = “dbSalary”;
   ```
   
   MySQL version >= 5.7 (password -> authentication_string)
   
   ```
   mysql> update user set authentication_string = password(“dbSalary”) where user = “dbSalary”;
   ```
   
   MySQL 8.0 (mysql 5.7.9 deprecated PASSWORD() function)
   
   ```
   mysql> flush privileges;
   mysql> update user set authentication_string = “” where user = “dbSalary”;
   mysql> ALTER user 'dbSalary'@'localhost' IDENTIFIED BY 'dbSalary';
   ```
4. Grant permission for user (so we can modify the database `dbSalary`)
   
   ```
   mysql> grant all privileges on *.* to 'dbSalary'@'localhost';
   ```
5. Restart MySQL

Note: the port `3306` is default for MySQL. You can change it in:

- Using Linux or Mac:
  File `.my.cnf` is usually located at `/etc/my.cnf` or `/etc/mysql/my.cnf`
- Using Windows:
  File `my.ini` is usually located at the installation directory

### Using Python to populate dataset into MySQL (small/test dataset)

There is a file called `startConnection.py` in the directory `code\py` that is created to help create and populate tables.
It will use the files in `./code/Database/schema` to drop and create table, and then populate the data from `./code/Database/testdb` into the MySQL database.

Once [setup](#Setup_Locally) is done, run `startConnection.py` will automatically create all the corresponding relations with existing entries inside.

Note: `startConnection.py` is using port `3306` to connect with MySQL.

Notice that the following code can be placed in MySQL Connection to create the tables in MySQL. (Also stored as a file in `./code/Database/mysql`). But as long as the setup section was successful, this would be optional.

```
create database if not exists dbSalary;
use dbSalary;
```

### Populate a large dataset into MySQL

Step 1: Download the salaryData.csv [(link)](https://data.louisvilleky.gov/sites/default/files/27096/SalaryData.csv)

Step 2: Put the dataset in directory `./source`. Save the file name as `SalaryData.csv`.

Step 3: Install python with a package called `pymysql` and configure MySQL settings as shown in [Setup Locally](#setup-locally).

Step 4: Run the shell `populateDatabase.sh`.

Now, all the data in this dataset will be populated into your MySQL databse.
*Note* that we only take the first 30k entries from the dataset. This can be modified in `./code/py/trasformDataset.py`.

### MySQL test samples

There are some test samples, including queries and outputs are in the directory `./code/Database` with the directory/folder name `text_xxx`.

### Running the application locally

Step 1: Download the salaryData.csv [link](https://data.louisvilleky.gov/sites/default/files/27096/SalaryData.csv)

Step 2: Put the dataset in directory `./source`. Keep its file name as `SalaryData.csv`.

Step 3: Install python with a package called `pymysql` and configure MySQL settings as shown in [Setup Locally](#setup-locally).

Step 4: Run the shell `startApplication.sh`.

Step 5: Turn on an brower and go to [http://localhost:6060/](http://localhost:6060/)

### Using Docker to run the application
<span id="setup_docker"></span>

If you have **docker** and **docker-compose** installed correctly, the only thing you need to do is to execute `docker-compose up` in your terminal (make sure you are in the right directory). Then docker should take care of everything for you. This might take a while (around half an hour for the first build) since docker will need to install and download several packages from the internet. Note: the image is around 500 MB.

After `docker-compose` is successfully started, you should be able to see two containers:

- `mysql_docker` (using port 3306) : this is the container for the mysql service, which is the database
- `database_app` (using port 6060) : this is the application

Then you can turn on a browser and go to [http://localhost:6060/](http://localhost:6060/) to check what the application can do (an augmented and specially-made version of SQL command `select` for this very dataset).

If you want to delete the containers for the service, you should type `docker-compose down`.
