#!/bin/bash

### Prepareation, transform the dataset
PYDIR="$(pwd)/code/py/"
TRANSFORMFILE="trasformDataset.py"

### Populate the code into the database
STARTFILE="startConnection.py"

cd $PYDIR
python $TRANSFORMFILE
python $STARTFILE

# read a key to quit
read -p "Press any key to continue... " -n1 -s