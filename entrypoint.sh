#!/bin/bash
### Prepareation, transform the dataset
CURRENTDIR="$(pwd)"
PYDIR="$(pwd)/code/py/"

cd $PYDIR
gunicorn server:app -c gunicorn.conf.py
cd $CURRENTDIR
source startApplication.sh