ARG PYTHON_VERSION=3.7

FROM python:${PYTHON_VERSION}-slim
WORKDIR /app
COPY . .
RUN pip --default-timeout=1000 install --no-cache-dir -r requirements.txt
# ENTRYPOINT ["entrypoint.sh"]