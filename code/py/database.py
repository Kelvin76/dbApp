import pymysql
import startConnection as startConn


class Database:

    _instance = None
    __first_init = True

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self):  # TODO
        if self.__first_init:
            self.__first_init = False
            # some other usefull tools can be found in connObject
            self.connObject = startConn.connect()
            # directly using the connection to the database
            self.conn = self.connObject.getConnection()
            self.pwd = ""
            self.limit = 1000

    def showAll(self, char, param1=""):
        db = self.conn

        if char == 'd':
            try:
                cursor = db.cursor()
                sql = """select distinct departmentName from department
                        order by departmentName"""
                cursor.execute(sql)
                data = cursor.fetchmany(self.limit)
                refineData = []
                for name in data:
                    nameRefine = name[0].replace(" ", "_")
                    if nameRefine not in refineData:
                        refineData.append(nameRefine)
                return refineData
            except Exception as e:
                print(format(e))
                return ["database searching error"]
        if char == 'j':
            try:
                cursor = db.cursor()
                sql = """select distinct jobTitle from hasJob
                        order by jobTitle"""
                cursor.execute(sql)
                data = cursor.fetchmany(self.limit)
                refineData = []
                for name in data:
                    nameRefine = name[0].replace(" ", "_")
                    if nameRefine not in refineData:
                        refineData.append(nameRefine)
                return refineData
            except Exception as e:
                print(format(e))
                return ["database searching error"]
        if char == 'y':
            try:
                cursor = db.cursor()
                sql = """select distinct year from hasSalary
                        order by year"""
                cursor.execute(sql)
                data = cursor.fetchmany(self.limit)
                refineData = []
                for name in data:
                    if str(name[0]) not in refineData:
                        refineData.append(str(name[0]))
                return refineData
            except Exception as e:
                print(format(e))
                return ["database searching error"]
        if char == 'seeIncentive':
            try:
                cursor = db.cursor()
                sql = """SELECT departmentRevenue.departmentName, ROUND(AVG(departmentRevenue.incentiveAllowence),2) AS Rounded
                FROM
                (SELECT s.incentiveAllowence incentiveAllowence,T.departmentName departmentName
                FROM(
                SELECT salaryID, departmentName
                FROM hasSalary,employeeDepartment
                WHERE hasSalary.employeeId = employeeDepartment.employeeId) as T, salary as s
                WHERE T.salaryID = s.salaryID) as departmentRevenue
                GROUP BY departmentRevenue.departmentName
                ORDER BY departmentRevenue.departmentName"""
                if(param1!= "all"):
                    sql = """SELECT departmentRevenue.departmentName, ROUND(AVG(departmentRevenue.incentiveAllowence),2) AS Rounded
                    FROM
                    (SELECT s.incentiveAllowence incentiveAllowence,T.departmentName departmentName
                    FROM(
                    SELECT salaryID, departmentName
                    FROM hasSalary,employeeDepartment
                    WHERE hasSalary.employeeId = employeeDepartment.employeeId) as T, salary as s
                    WHERE T.salaryID = s.salaryID) as departmentRevenue
                    WHERE departmentRevenue.departmentName =  \""""+param1+"""\"
                    GROUP BY departmentRevenue.departmentName
                    ORDER BY departmentRevenue.departmentName"""
                cursor.execute(sql)
                data = [("Department Name", "Average Incentive")]
                result = cursor.fetchmany(self.limit)
                if len(result) != 0:
                    data += result
                else:
                    data += [("Found zero result", "")]
                return data
            except Exception as e:
                print(format(e))
                return ["database searching error"]
        if char == 'seeAverageSalary':
            try:
                cursor = db.cursor()
                sql = """SELECT departmentRevenue.departmentName, ROUND(AVG(departmentRevenue.annualRate),2) AS Rounded
                FROM
                (SELECT s.annualRate annualRate,T.departmentName departmentName
                FROM(
                SELECT salaryID, departmentName
                FROM hasSalary,employeeDepartment
                WHERE hasSalary.employeeId = employeeDepartment.employeeId) as T, salary as s
                WHERE T.salaryID = s.salaryID) as departmentRevenue
                GROUP BY departmentRevenue.departmentName
                ORDER BY departmentRevenue.departmentName;"""
                if(param1 != "all"):
                    sql = """SELECT departmentRevenue.departmentName, ROUND(AVG(departmentRevenue.annualRate),2) AS Rounded
                    FROM
                    (SELECT s.annualRate annualRate,T.departmentName departmentName
                    FROM(
                    SELECT salaryID, departmentName
                    FROM hasSalary,employeeDepartment
                    WHERE hasSalary.employeeId = employeeDepartment.employeeId) as T, salary as s
                    WHERE T.salaryID = s.salaryID) as departmentRevenue
                    WHERE departmentRevenue.departmentName = \""""+param1+"""\"
                    GROUP BY departmentRevenue.departmentName
                    ORDER BY departmentRevenue.departmentName;"""
                cursor.execute(sql)
                data = [("Department Name", "Average Annual Income")]
                result = cursor.fetchmany(self.limit)
                if len(result) != 0:
                    data += result
                else:
                    data += [("Found zero result", "")]
                return data
            except Exception as e:
                print(format(e))
                return ["database searching error"]
        if char == 'seeDepartmentSize':
            try:
                cursor = db.cursor()
                sql = """SELECT departmentName,COUNT(*) as size
                FROM employeeDepartment
                GROUP BY departmentName
                ORDER BY size DESC;"""
                if(param1 != "all"):
                    sql = """SELECT departmentName,COUNT(*) as size
                    FROM employeeDepartment
                    WHERE departmentName = \""""+param1+"""\"
                    GROUP BY departmentName
                    ORDER BY size DESC;"""
                cursor.execute(sql)
                data = [("Department Name", "Department Size")]
                result = cursor.fetchmany(self.limit)
                if len(result) != 0:
                    data += result
                else:
                    data += [("Found zero result", "")]
                return data
            except Exception as e:
                print(format(e))
                return ["database searching error"]
        if char == 'seeHighestSalary':
            try:
                cursor = db.cursor()
                sql = """SELECT departmentRevenue.departmentName, Max(departmentRevenue.annualRate)
                FROM
                (SELECT s.annualRate annualRate,T.departmentName departmentName
                FROM(
                SELECT salaryID, departmentName
                FROM hasSalary,employeeDepartment
                WHERE hasSalary.employeeId = employeeDepartment.employeeId) as T, salary as s
                WHERE T.salaryID = s.salaryID) as departmentRevenue
                GROUP BY departmentRevenue.departmentName
                ORDER BY departmentRevenue.departmentName;
                """
                if(param1 != "all"):
                    sql = """SELECT departmentRevenue.departmentName, Max(departmentRevenue.annualRate)
                    FROM
                    (SELECT s.annualRate annualRate,T.departmentName departmentName
                    FROM(
                    SELECT salaryID, departmentName
                    FROM hasSalary,employeeDepartment
                    WHERE hasSalary.employeeId = employeeDepartment.employeeId) as T, salary as s
                    WHERE T.salaryID = s.salaryID) as departmentRevenue
                    WHERE departmentRevenue.departmentName = \""""+param1+"""\"
                    GROUP BY departmentRevenue.departmentName
                    ORDER BY departmentRevenue.departmentName;
                    """
                cursor.execute(sql)
                data = [("Department Name", "Max Salary")]
                result = cursor.fetchmany(self.limit)
                if len(result) != 0:
                    data += result
                else:
                    data += [("Found zero result", "")]
                return data
            except Exception as e:
                print(format(e))
                return ["database searching error"]
        if char == 'incentiveAllowance':
            try:
                cursor = db.cursor()
                sql = """SELECT employeeID, employeeName, incentiveAllowence, year
                FROM employee
                LEFT OUTER JOIN(
                    SELECT employeeID AS empID, incentiveAllowence, year
                    FROM hasSalary AS has1
                    LEFT OUTER JOIN(
                        SELECT salaryID AS salID1,  incentiveAllowence
                        FROM salary AS s1
                        WHERE s1.incentiveAllowence > 0) AS s2
                        ON has1.salaryID = s2.salID1
                    ) AS has2
                ON employee.employeeID = has2.empID
                ORDER BY employeeID ASC;"""

                if(param1 != "all"):
                    sql = """SELECT employeeID, employeeName, incentiveAllowence, year
                    FROM employee
                    LEFT OUTER JOIN(
                        SELECT employeeID AS empID, incentiveAllowence, year
                        FROM hasSalary AS has1
                        LEFT OUTER JOIN(
                            SELECT salaryID AS salID1,  incentiveAllowence
                            FROM salary AS s1
                            WHERE s1.incentiveAllowence > 0) AS s2
                            ON has1.salaryID = s2.salID1
                        ) AS has2
                    ON employee.employeeID = has2.empID
                    WHERE employee.employeeName = \""""+param1+"""\"
                    ORDER BY employeeID ASC;"""

                cursor.execute(sql)
                data = [("Employee ID", "Employee Name",
                         "Incentive Allowence", "Year")]
                result = cursor.fetchmany(self.limit)

                if len(result) != 0:
                    data += result
                else:
                    data += [("Found zero result", "","","")]
                return data
            except Exception as e:
                print(format(e))
                return ["database searching error"]
        if char == 'seeWorkHistory':
            try:
                cursor = db.cursor()
                sql = """SELECT employeeID, employeeName, jobTitle, departmentName, empl2.year
                FROM jobDepartment
                RIGHT OUTER JOIN(
                    SELECT employeeID, employeeName, jobTitle AS jt,year
                    FROM hasJob
                    RIGHT OUTER JOIN(
                        SELECT employeeID AS empID, employeeName 
                        FROM employee
                        WHERE employeeName =\""""+param1+"""\"
                    ) AS empl1
                    ON hasJob.employeeID = empl1.empID
                ) AS empl2
                ON jobDepartment.jobTitle = empl2.jt
                GROUP BY empl2.year
                ORDER BY empl2.year ASC;"""
                cursor.execute(sql)
                data = [("Employee ID", "Employee Name", "Job Title", "Department Name",
                         "Year")]
                result = cursor.fetchmany(self.limit)
                if len(result) != 0:
                    data += result
                else:
                    data += [("Found zero result", "","", "", "")]
                return data
            except Exception as e:
                print(format(e))
                return ["database searching error"]
        if char == 'seeSomeEmployeeName':
            try:
                cursor = db.cursor()
                sql = """SELECT employeeName
                FROM employee
                WHERE employeeName LIKE \"""" + param1 + """%\"
                ORDER BY employeeName ASC"""
                cursor.execute(sql)
                data = [("Employee Name",)]
                result = cursor.fetchmany(self.limit)
                if len(result) != 0:
                    data += result
                else:
                    data += [("Found zero result",)]
                return data
            except Exception as e:
                print(format(e))
                return ["database searching error"]

    def searchDepartment(self, year=None, depart=None, job=None):
        db = self.conn
        try:
            cursor = db.cursor()
            sql = ""
            data = []
            if(depart is not None):
                sql = " select e.employeeName, ed.departmentName, ed.year from employeeDepartment AS ed, employee AS e where "
                sql += "ed.year = "+year+" and "
                sql += "ed.departmentName = \""+depart+"\" and "
                sql += "e.employeeID = ed.employeeID order by e.employeeName"
                data = [("Employee Name", "Department Name", "Year")]
            elif(job is not None):
                sql = " select em.employeeName, has.jobTitle, has.year from hasJob AS has, employee AS em where "
                sql += "has.year = "+year+" and "
                sql += "has.jobTitle = \""+job+"\" and "
                sql += "em.employeeID = has.employeeID"
                data = [("Employee Name", "Job Title", "Year")]
            cursor.execute(sql)
            result = cursor.fetchmany(self.limit)
            if len(result) != 0:
                data += result
            else:
                data += [("Found zero result", "", "")]
            return data
        except Exception as e:
            print(format(e))
            db.close()
            return ["database searching error"]

    def search(self, table, order_by, attribute):
        db = self.conn

        # make sure inputs are valid
        if(table not in self.connObject.tableNames):
            return ["database searching error: table name does not exists"]
        if(order_by not in self.connObject.attributes[table]):
            return ["database searching error: attribute does not exists"]
        if(attribute != "*" or attribute not in self.connObject.attributes[table]):
            return ["database searching error: order attribute does not exists"]

        try:
            cursor = db.cursor()
            sql = "select " + attribute + " from " + table
            sql += "order by " + order_by
            cursor.execute(sql)
            data = cursor.fetchmany(self.limit)
            return data
        except Exception as e:
            print(format(e))
            db.close()
            return ["database searching error"]

    def insert(self, table, sa_id, year, em_id, name, department, job,
               annual, regular, overtime, allow, other, date, pwd):
        if pwd != self.pwd:
            return "password is not correct"
        db = self.conn
        try:
            cursor = db.cursor()
            sql = ""  # TODO
            cursor.execute(sql)
            db.commit()
            return "OK"
        except Exception as e:
            print(format(e))
            db.rollback()
            db.close()
            return "database inserting error"

    def delete(self, table,  did, pwd):
        if pwd != self.pwd:
            return "password is not correct"
        db = self.conn
        try:
            cursor = db.cursor()
            sql = ""  # TODO
            cursor.execute(sql)
            db.commit()
            return "OK"
        except Exception as e:
            print(format(e))
            db.rollback()
            db.close()
            return "database deleting error"
