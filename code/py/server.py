from flask import Flask, request, render_template

from database import Database

app = Flask(__name__)


@app.route('/seeIncentive', methods=['GET', 'POST'])
def seeIncentive():
    database = Database()
    departmentName = request.form.get('DepartmentName')
    return render_template('index.html', show=database.showAll("seeIncentive", departmentName), departmentSelection=["None"] + database.showAll('d'),
                           info="", years=database.showAll('y'), jobs=["None"]+database.showAll('j'))


@app.route('/seeAverageSalary', methods=['GET', 'POST'])
def seeAverageSalary():
    database = Database()
    departmentName = request.form.get('DepartmentName')
    return render_template('index.html', show=database.showAll("seeAverageSalary", departmentName), departmentSelection=["None"] + database.showAll('d'),
                           info="", years=database.showAll('y'), jobs=["None"]+database.showAll('j'))


@app.route('/seeDepartmentSize', methods=['GET', 'POST'])
def seeDepartmentSize():
    database = Database()
    departmentName = request.form.get('DepartmentName')
    return render_template('index.html', show=database.showAll("seeDepartmentSize", departmentName), departmentSelection=["None"] + database.showAll('d'),
                           info="", years=database.showAll('y'), jobs=["None"]+database.showAll('j'))


@app.route('/seeHighestSalary', methods=['GET', 'POST'])
def seeHighestSalary():
    database = Database()
    departmentName = request.form.get('DepartmentName')
    return render_template('index.html', show=database.showAll("seeHighestSalary", departmentName), departmentSelection=["None"] + database.showAll('d'),
                           info="", years=database.showAll('y'), jobs=["None"]+database.showAll('j'))


@app.route('/incentiveAllowance', methods=['GET', 'POST'])
def incentiveAllowance():
    database = Database()
    employeeName = request.form.get('EmployeeName')
    return render_template('index.html', show=database.showAll("incentiveAllowance", employeeName), departmentSelection=["None"] + database.showAll('d'),
                           info="", years=database.showAll('y'), jobs=["None"]+database.showAll('j'))


@app.route('/seeWorkHistory', methods=['GET', 'POST'])
def seeWorkHistory():
    database = Database()
    employeeName = request.form.get('EmployeeName')
    return render_template('index.html', show=database.showAll("seeWorkHistory", employeeName), departmentSelection=["None"] + database.showAll('d'),
                           info="", years=database.showAll('y'), jobs=["None"]+database.showAll('j'))


@app.route('/seeSomeEmployeeName', methods=['GET', 'POST'])
def seeSomeEmployeeName():
    database = Database()
    nameInitial = request.form.get('nameInitial')
    return render_template('index.html', show=database.showAll("seeSomeEmployeeName", nameInitial), departmentSelection=["None"] + database.showAll('d'),
                           info="", years=database.showAll('y'), jobs=["None"]+database.showAll('j'))


@app.route('/', methods=['GET', 'POST'])
def index():
    database = Database()

    infoDisplay = "You can search whether \"Year + Department\" to see the employees working in that department at that year."
    infoDisplay += "Or you can search \"Year + Job\" to see who is serving as that job in that year"
    searchDe = []
    if request.method == 'POST':
        year = request.form.get('year')

        if (request.form.get('department') != ""):
            department = request.form.get('department')
            temp = department
            department = temp.replace("_", " ")
            if department == "None":
                department = None
        else:
            department = None

        if (request.form.get('job') != ""):
            job = request.form.get('job')
            temp = job
            job = temp.replace("_", " ")
            if job == "None":
                job = None
        else:
            job = None

        if department == None and job == None:
            infoDisplay = "You can search whether \"Year + Department\" to see the employees working in that department at that year."
            infoDisplay += "Or you can search \"Year + Job\" to see who is serving as that job in that year"
        else:
            infoDisplay = ""
            searchDe = database.searchDepartment(year, department, job)
    return render_template('index.html', show=searchDe, departmentSelection=["None"] + database.showAll('d'),
                           info=infoDisplay, jobs=["None"]+database.showAll('j'), years=database.showAll('y'))


@app.route('/admin', methods=['GET'])
def admin():
    return render_template('admin.html')


@app.route('/insert', methods=['POST'])
def insert():
    database = Database()
    sa_id = request.form.get('sa-id')
    year = request.form.get('year')
    em_id = request.form.get('em_id')
    name = request.form.get('name')
    department = request.form.get('department')
    job = request.form.get('job')
    annual = request.form.get('annual')
    regular = request.form.get('regular')
    overtime = request.form.get('overtime')
    allow = request.form.get('allow')
    other = request.form.get('other')
    date = request.form.get('date')
    pwd = request.form.get('pwd')
    if sa_id == "" or year == "" or em_id == "" or name == "" or \
            department == "" or job == "" or annual == "" or regular == "" or \
            overtime == "" or allow == "" or other == "" or date == "":
        r = "cannot be empty"
        return render_template('admin.html', insertResult=r)
    r = database.insert(sa_id, year, em_id, name, department,
                        job, annual, regular, overtime, allow,
                        other, date, pwd)
    return render_template('admin.html', insertResult=r)


@app.route('/delete', methods=['POST'])
def delete():
    database = Database()
    did = request.form.get('did')
    pwd = request.form.get('pwd')
    if did == "":
        r = "no delete id"
        return render_template('admin.html', deleteResult=r)
    r = database.delete(did, pwd)
    return render_template('admin.html', deleteResult=r)


if __name__ == '__main__':
    app.run(debug=False, port=6060)
