import csv

sourcePath = "../../source/SalaryData.csv"
outputPath = "../Database/transformed_data/"

lineNumber = 30000

salaryLines = []
employeeLines = []
jobLines = []
departmentLines = []
hasSalaryLines = []
hasJobLines = []
employeeDepartmentLines = []
jobDepartmentLines = []

employeeID = 0
employeeIDIndex = 0
employeeIDMap = {}

with open(sourcePath) as csvfile:
    allLines = list(csv.DictReader(csvfile))
    i = 0
    for line in allLines:
        i += 1

        # so 0 for lineNumber means take all the data
        if i > lineNumber or (lineNumber <= 0 and i >= len(allLines)):
            break

        employee = line['EmployeeName']
        if employee in employeeIDMap:
            employeeID = employeeIDMap[employee]
        else:
            employeeIDIndex += 1
            employeeIDMap[employee] = employeeIDIndex
            employeeID = employeeIDIndex

        salaryLines.append(line['SalaryDataID'].replace(',', '').strip() + ',' +
                           line['AnnualRate'].replace(',', '').strip() + ',' +
                           line['RegularRate'].replace(',', '').strip() + ',' +
                           line['OvertimeRate'].replace(',', '').strip() + ',' +
                           line['IncentiveAllowance'].replace(',', '').strip() + ',' +
                           line['Other'].replace(',', '').strip() + ',' +
                           line['YearToDate'].replace(',', '').strip())

        employeeLines.append(str(employeeID) + ',' + "\"" +
                             line['EmployeeName'].replace(',', '').strip() + "\"")

        jobLines.append(line['JobTitle'].replace(',', '').strip())

        departmentLines.append(line['Department'].replace(',', '').strip())

        hasSalaryLines.append(line['SalaryDataID'].replace(',', '').strip()+',' +
                              str(employeeID) + ',' +
                              line['CalendarYear'].replace(',', '').strip())

        hasJobLines.append(str(employeeID)+',' +
                           line['JobTitle'].replace(',', '').strip() + ',' +
                           line['CalendarYear'].replace(',', '').strip())

        employeeDepartmentLines.append(str(employeeID)+',' +
                                       line['Department'].replace(',', '').strip() + ',' +
                                       line['CalendarYear'].replace(',', '').strip())

        jobDepartmentLines.append(line['JobTitle'].replace(',', '').strip() + ',' +
                                  line['Department'].replace(',', '').strip())


def reduceAndWrite(cols, lines, name):
    reducedLines = set(lines)
    linesString = ''
    for line in reducedLines:
        s = line + '\n'
        linesString += s
    f = open(name, 'w')
    f.write(cols+'\n'+linesString)
    f.close()


reduceAndWrite("salaryId,annualRate,regularRate,overtimeRate,incentiveAllowence,other,yearToDate",
               salaryLines, outputPath+'salary.txt')
reduceAndWrite("employeeID,employeeName",
               employeeLines, outputPath+'employee.txt')
reduceAndWrite("jobTitle", jobLines, outputPath+'job.txt')
reduceAndWrite("departmentName", departmentLines, outputPath+'department.txt')
reduceAndWrite("salaryId,employeeID,year",
               hasSalaryLines, outputPath+'hasSalary.txt')
reduceAndWrite("employeeID,jobTitle,year",
               hasJobLines, outputPath+'hasJob.txt')
reduceAndWrite("employeeID,departmentName,year",
               employeeDepartmentLines, outputPath+'employeeDepartment.txt')
reduceAndWrite("jobTitle,departmentName", jobDepartmentLines,
               outputPath+'jobDepartment.txt')
