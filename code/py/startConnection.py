import traceback
from sqlalchemy import create_engine
import os
import socket
import pymysql
import pandas as pd
import numpy as np
import warnings

sqlDistinguisher = "$$$"
dropTableFile = "../Database/schema/droptables.sql"
createTableFile = "../Database/schema/createtables.sql"
dropIndexFile = "../Database/schema/dropindices.sql"
createIndexFile = "../Database/schema/createindices.sql"
sampleDatasetPath = "../Database/transformed_data/"
dataFileNames = ['department.txt', 'job.txt', 'salary.txt', 'employee.txt',
                 'employeeDepartment.txt', 'hasJob.txt', 'hasSalary.txt', 'jobDepartment.txt', ]


class Connection:
    """
    The class of mysql connection.

    param host: ip for mysql
    param user: username
    param password: password
    param dbName: the database used
    param port: port (default 3306)

    :type host: ip for mysql
    :type user: username
    :type password: password
    :type db: the database used
    :type port: port (default 3306)
    :type conn: connection
    :type cur: cursor
    """

    __instance = None
    __first_init = True

    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = super().__new__(cls)
        return cls.__instance

    def __init__(self, host, user, password, db, port=3306, charset='utf8'):
        if self.__first_init:
            self.__first_init = False
            self.__host = host
            self.__user = user
            self.__password = password
            self.__db = db
            self.__port = port
            self.__charset = charset
            self.__conn = pymysql.connect(
                host=host,
                user=user,
                password=password,
                db=db,
                port=port,
                charset=charset)
            self.__cur = self.__conn.cursor()
            self.tableNames = []
            self.attributes = {}

            # create tables
            try:
                # drop tables before create
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    # If run for the first time, cursor will warn us that tables do not exist
                    with open(dropTableFile, 'r') as f:
                        sqlCommands = f.read()
                        tables = sqlCommands.split(sqlDistinguisher)
                        for table in tables:
                            self.__cur.execute(table)
                        self.__conn.commit()
                # # create tables
                with open(createTableFile, 'r') as f:
                    sqlCommands = f.read()
                    tables = sqlCommands.split(sqlDistinguisher)
                    for table in tables:
                        self.__cur.execute(table)
                    self.__conn.commit()
                # # create indices
                with open(createIndexFile, 'r') as f:
                    sqlCommands = f.read()
                    tables = sqlCommands.split(sqlDistinguisher)
                    for table in tables:
                        self.__cur.execute(table)
                    self.__conn.commit()

                # populate the data in
                # sample data (small dataset)
                # get table names:

                fileNames = dataFileNames

                self.tableNames = [x.replace(".txt", "") for x in fileNames]

                # get file paths for the data
                datasetPathList = [sampleDatasetPath + x for x in fileNames]

                # populate the data
                for i in range(0, len(self.tableNames)):
                    self.csv2tables(datasetPathList[i], self.tableNames[i])

            except Exception as e:
                print("Error:", e)
                self.__conn.rollback()
            self.__conn.commit()

    def getConnection(self):
        return self.__conn

    def csv2tables(self, fileName, tableName):

        # connect database
        self.__conn.select_db(self.__db)

        df = pd.read_csv(fileName, sep=',', dtype=np.str)

        print("====== Starting to Populate data into database {0} , in {1} ======".format(
            tableName, self.__db))

        values = df.values.tolist()
        # based on column numbers
        s = ','.join(['%s' for _ in range(len(df.columns))])

        self.attributes[tableName] = list(df.columns)

        # executemany
        self.__cur.executemany(
            'INSERT INTO {} VALUES ({})'.format(tableName, s), values)
        self.__conn.commit()
        print("====== ---------- Populating data finished ---------- ======")

    def __del__(self):
        self.exit()

    def exit(self):
        self.__cur.close()
        self.__conn.close()
        return

    def mainMenu(self):
        return


# return the connection object
# the object has a specified hostname (mysql_service) if using docker
def connect():
    if(socket.gethostname() == "docker_app"):
        return Connection(
            host='mysql_service',
            user='dbSalary',
            password='dbSalary',
            db='dbSalary',
            port=3306,
            charset='utf8')
    return Connection(
        host='localhost',
        user='dbSalary',
        password='dbSalary',
        db='dbSalary',
        port=3306,
        charset='utf8')


def __main__():
    c = connect()
    return


if __name__ == '__main__':
    __main__()
    pass
