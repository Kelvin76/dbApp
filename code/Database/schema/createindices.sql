CREATE INDEX IX_hsEID
ON hasSalary(employeeID);
$$$
CREATE INDEX IX_hsSID
ON hasSalary(salaryID);
$$$
CREATE INDEX IX_hjEID
ON hasJob(employeeID);
$$$
CREATE INDEX IX_hjJTITLE
ON hasJob(jobTitle);
$$$
CREATE INDEX IX_jdJTITLE
ON jobDepartment(jobTitle);
$$$
CREATE INDEX IX_edDNAME
ON employeeDepartment(departmentName);