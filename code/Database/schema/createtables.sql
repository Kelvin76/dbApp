CREATE TABLE salary
(
   salaryId DECIMAL(9, 0) NOT NULL PRIMARY KEY,
   annualRate DECIMAL(9, 2),
   regularRate DECIMAL(9, 2),
   overtimeRate DECIMAL(9, 2),
   incentiveAllowence DECIMAL(9, 2),
   other DECIMAL(9, 2),
   yearToDate DECIMAL(9, 2)
);
$$$
CREATE TABLE employee
(
   employeeID DECIMAL(9, 0) NOT NULL PRIMARY KEY,
   employeeName VARCHAR(30) NOT NULL
);
$$$
CREATE TABLE job
(
   jobTitle VARCHAR(30) NOT NULL PRIMARY KEY
);
$$$
CREATE TABLE department
(
   departmentName VARCHAR(30) NOT NULL PRIMARY KEY
);
$$$
CREATE TABLE hasSalary
(
   salaryId DECIMAL(9, 0) NOT NULL,
   employeeID DECIMAL(9, 0) NOT NULL,
   year DECIMAL(4, 0) NOT NULL,
   PRIMARY KEY(salaryId, employeeID, year),
   FOREIGN KEY(salaryId) REFERENCES salary(salaryId),
   FOREIGN KEY(employeeID) REFERENCES employee(employeeID)
);
$$$
CREATE TABLE hasJob
(
   employeeID DECIMAL(9, 0) NOT NULL,
   jobTitle VARCHAR(30) NOT NULL,
   year DECIMAL(4, 0) NOT NULL,
   PRIMARY KEY(employeeID, jobTitle, year),
   FOREIGN KEY(employeeID) REFERENCES employee(employeeID),
   FOREIGN KEY(jobTitle) REFERENCES job(jobTitle)
);
$$$
CREATE TABLE employeeDepartment
(
   employeeID DECIMAL(9, 0) NOT NULL,
   departmentName VARCHAR(30) NOT NULL,
   year DECIMAL(4, 0) NOT NULL,
   PRIMARY KEY(employeeID, departmentName,year),
   FOREIGN KEY(departmentName) REFERENCES department(departmentName)
);
$$$
CREATE TABLE jobDepartment
(
   jobTitle VARCHAR(30) NOT NULL,
   departmentName VARCHAR(30) NOT NULL,
   PRIMARY KEY(jobTitle, departmentName),
   FOREIGN KEY(departmentName) REFERENCES department(departmentName),
   FOREIGN KEY(jobTitle) REFERENCES job(jobTitle)
);


