-- Query for workHistory of employee (Ordered and grouped by year)
use dbSalary;

SELECT employeeID, jobTitle, departmentName, empl2.year
FROM jobDepartment
LEFT OUTER JOIN(
    SELECT employeeID, employeeName, jobTitle AS jt,year
    FROM hasJob
    LEFT OUTER JOIN(
        SELECT employeeID AS empID, employeeName 
        FROM employee
        WHERE employeeName = "Girten Terrence"
    ) AS empl1
    ON hasJob.employeeID = empl1.empID
) AS empl2
ON jobDepartment.jobTitle = empl2.jt
GROUP BY empl2.year
ORDER BY empl2.year ASC;
