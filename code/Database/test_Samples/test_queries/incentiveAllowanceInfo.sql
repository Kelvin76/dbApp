-- Query for information on incentive allowance
use dbSalary;

SELECT employeeID, employeeName, incentiveAllowence, year
FROM employee
LEFT OUTER JOIN(
    SELECT employeeID AS empID, incentiveAllowence, year
    FROM hasSalary AS has1
    LEFT OUTER JOIN(
        SELECT salaryID AS salID1,  incentiveAllowence
        FROM salary AS s1
        WHERE s1.incentiveAllowence > 0) AS s2
        ON has1.salaryID = s2.salID1
    ) AS has2
ON employee.employeeID = has2.empID
ORDER BY employeeID ASC;
