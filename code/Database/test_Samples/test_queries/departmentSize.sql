-- Query for department size
use dbSalary;

SELECT departmentName,COUNT(*) as size
FROM employeeDepartment
GROUP BY departmentName
ORDER BY size DESC;