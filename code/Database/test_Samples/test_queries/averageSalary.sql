-- Query for average annual rate of each department 
use dbSalary;

SELECT departmentRevenue.departmentName, AVG(departmentRevenue.annualRate)
FROM
(SELECT s.annualRate annualRate,T.departmentName departmentName
FROM(
SELECT salaryID, departmentName
FROM hasSalary,employeeDepartment
WHERE hasSalary.employeeId = employeeDepartment.employeeId) as T, salary as s
WHERE T.salaryID = s.salaryID) as departmentRevenue
GROUP BY departmentRevenue.departmentName
ORDER BY departmentRevenue.departmentName;


