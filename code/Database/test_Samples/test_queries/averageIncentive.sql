-- Query for average incentiveAllowance of each department 
use dbSalary;

SELECT departmentRevenue.departmentName, AVG(departmentRevenue.incentiveAllowence)
FROM
(SELECT s.incentiveAllowence incentiveAllowence,T.departmentName departmentName
FROM(
SELECT salaryID, departmentName
FROM hasSalary,employeeDepartment
WHERE hasSalary.employeeId = employeeDepartment.employeeId) as T, salary as s
WHERE T.salaryID = s.salaryID) as departmentRevenue
GROUP BY departmentRevenue.departmentName
ORDER BY departmentRevenue.departmentName
LIMIT 10;
