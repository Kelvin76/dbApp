-- test-production, containing all the sql statements (with limit of 10 entries)

use dbSalary;

-- Query for average incentiveAllowance of each department
SELECT departmentRevenue.departmentName, AVG(departmentRevenue.incentiveAllowence)
FROM
(SELECT s.incentiveAllowence incentiveAllowence,T.departmentName departmentName
FROM(
SELECT salaryID, departmentName
FROM hasSalary,employeeDepartment
WHERE hasSalary.employeeId = employeeDepartment.employeeId) as T, salary as s
WHERE T.salaryID = s.salaryID) as departmentRevenue
GROUP BY departmentRevenue.departmentName
ORDER BY departmentRevenue.departmentName
LIMIT 10;

-- Query for average annual rate of each department 
SELECT departmentRevenue.departmentName, AVG(departmentRevenue.annualRate)
FROM
(SELECT s.annualRate annualRate,T.departmentName departmentName
FROM(
SELECT salaryID, departmentName
FROM hasSalary,employeeDepartment
WHERE hasSalary.employeeId = employeeDepartment.employeeId) as T, salary as s
WHERE T.salaryID = s.salaryID) as departmentRevenue
GROUP BY departmentRevenue.departmentName
ORDER BY departmentRevenue.departmentName
LIMIT 10;

-- Query for department size
SELECT departmentName,COUNT(*) as size
FROM employeeDepartment
GROUP BY departmentName
ORDER BY size DESC
LIMIT 10;

-- Query for the max annual rate of each department 
SELECT departmentRevenue.departmentName, Max(departmentRevenue.annualRate)
FROM
(SELECT s.annualRate annualRate,T.departmentName departmentName
FROM(
SELECT salaryID, departmentName
FROM hasSalary,employeeDepartment
WHERE hasSalary.employeeId = employeeDepartment.employeeId) as T, salary as s
WHERE T.salaryID = s.salaryID) as departmentRevenue
GROUP BY departmentRevenue.departmentName
ORDER BY departmentRevenue.departmentName
LIMIT 10;

-- Query for information on incentive allowance
SELECT employeeID, employeeName, incentiveAllowence, year
FROM employee
LEFT OUTER JOIN(
    SELECT employeeID AS empID, incentiveAllowence, year
    FROM hasSalary AS has1
    LEFT OUTER JOIN(
        SELECT salaryID AS salID1,  incentiveAllowence
        FROM salary AS s1
        WHERE s1.incentiveAllowence > 0) AS s2
        ON has1.salaryID = s2.salID1
    ) AS has2
ON employee.employeeID = has2.empID
ORDER BY employeeID ASC
LIMIT 10;

-- Query for workHistory of employee (Ordered and grouped by year)
SELECT employeeID, jobTitle, departmentName, empl2.year
FROM jobDepartment
LEFT OUTER JOIN(
    SELECT employeeID, employeeName, jobTitle AS jt,year
    FROM hasJob
    LEFT OUTER JOIN(
        SELECT employeeID AS empID, employeeName 
        FROM employee
        WHERE employeeName = "Girten Terrence"
    ) AS empl1
    ON hasJob.employeeID = empl1.empID
) AS empl2
ON jobDepartment.jobTitle = empl2.jt
GROUP BY empl2.year
ORDER BY empl2.year ASC
LIMIT 10;

