## test-production

`test-production.sql` contains all the queries in `../test_Samples/test_queries` with an addition of `LIMIT 10` constraint at the end.

`test-production_out_x.csv` are the ooutput for `test-production.sql` from the large dataset.