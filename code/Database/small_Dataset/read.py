import csv

sourcePath = "../../../source/SalaryData.csv"
outputPath = "./"

lineNumber = 50

salaryLines = []
employeeLines = []
jobLines = []
departmentLines = []
hasSalaryLines = []
hasJobLines = []
employeeDepartmentLines = []
jobDepartmentLines = []

employeeID = 0
employeeIDIndex = 0
employeeIDMap = {}

with open(sourcePath) as csvfile:
	allLines = csv.DictReader(csvfile)
	i = 0
	for line in allLines:
		i += 1
		if i > lineNumber:
			break

		employee = line['EmployeeName']
		if employee in employeeIDMap:
			employeeID = employeeIDMap[employee]
		else:
			employeeIDIndex += 1
			employeeIDMap[employee] = employeeIDIndex
			employeeID = employeeIDIndex

		salaryLines.append(line['SalaryDataID'].strip() +',' + 
				line['AnnualRate'].strip() + ',' + 
				line['RegularRate'].strip() + ',' + 
				line['OvertimeRate'].strip() + ',' + 
				line['IncentiveAllowance'].strip() + ',' + 
				line['Other'].strip() + ',' + 
				line['YearToDate'].strip())

		employeeLines.append(str(employeeID) + ',' + "\""+
				line['EmployeeName'].strip() + "\"")

		jobLines.append(line['JobTitle'].strip())

		departmentLines.append(line['Department'].strip())

		hasSalaryLines.append(line['SalaryDataID'].strip()+',' + 
				str(employeeID) + ',' + 
				line['CalendarYear'].strip())
		
		hasJobLines.append(str(employeeID)+',' + 
				line['JobTitle'].strip() + ',' + 
				line['CalendarYear'].strip())
		
		employeeDepartmentLines.append(str(employeeID)+',' + 
				line['Department'].strip() + ',' + 
				line['CalendarYear'].strip())
		
		jobDepartmentLines.append(line['JobTitle'].strip() + ',' + 
				line['Department'].strip())

def reduceAndWrite(cols, lines, name):
	reducedLines = set(lines)
	linesString = ''
	for line in reducedLines:
		s = line + '\n'
		linesString += s
	f = open(name, 'w')
	f.write(cols+'\n'+linesString)
	f.close()

reduceAndWrite("salaryId,annualRate,regularRate,overtimeRate,incentiveAllowence,other,yearToDate",salaryLines, outputPath+'salary.txt')
reduceAndWrite("employeeID,employeeName",employeeLines, outputPath+'employee.txt')
reduceAndWrite("jobTitle",jobLines, outputPath+'job.txt')
reduceAndWrite("departmentName",departmentLines, outputPath+'department.txt')
reduceAndWrite("salaryId,employeeID,year",hasSalaryLines, outputPath+'hasSalary.txt')
reduceAndWrite("employeeID,jobTitle,year",hasJobLines, outputPath+'hasJob.txt')
reduceAndWrite("employeeID,departmentName,year",employeeDepartmentLines, outputPath+'employeeDepartment.txt')
reduceAndWrite("jobTitle,departmentName",jobDepartmentLines, outputPath+'jobDepartment.txt')